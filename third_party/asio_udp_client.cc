#include <array>
#include <iostream>

#include <asio/ip/udp.hpp>

int main() {
    asio::io_context context;
    asio::ip::udp::resolver resolver(context);
    asio::ip::udp::endpoint receiverEndpoint = *resolver.resolve(asio::ip::udp::v4(), "127.0.0.1", "daytime").begin();

    asio::ip::udp::socket socket(context);
    socket.open(asio::ip::udp::v4());

    std::array<char, 1> sendBuf{'a'};
    socket.send_to(asio::buffer(sendBuf), receiverEndpoint);

    std::array<char, 128> recvBuf{};
    asio::ip::udp::endpoint senderEndpoint;
    size_t len = socket.receive_from(asio::buffer(recvBuf), senderEndpoint);
    std::cout << recvBuf.data() << std::endl;

    return 0;
}
