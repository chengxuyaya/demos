#include <iostream>

#include <asio/steady_timer.hpp>
#include <asio/placeholders.hpp>

class Printer {
public:
    Printer()
        : context_(), timer_(context_, asio::chrono::seconds(1)), count_(0) {
        timer_.async_wait(std::bind(&Printer::print, this));
    }
    ~Printer() {
        std::cout << "final count is " << count_ << std::endl;
    }

    void print() {
        if (count_ < 5) {
            std::cout << count_++ << std::endl;
            timer_.expires_at(timer_.expiry() + asio::chrono::seconds(1));
            timer_.async_wait(std::bind(&Printer::print, this));
        }
    }

    void run() {
        context_.run();
    }

private:
    asio::io_context context_;
    asio::steady_timer timer_;
    int count_;
};

int main() {
    Printer printer;
    printer.run();

    return 0;
}
