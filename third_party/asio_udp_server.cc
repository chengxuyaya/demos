#include <array>
#include <ctime>
#include <iostream>
#include <string>

#include <asio/ip/udp.hpp>
#include <asio/placeholders.hpp>

std::string makeDayTime() {
    time_t now = std::time(0);
    return std::ctime(&now);
}

class UDPServer {
public:
    UDPServer()
        : context_(), socket_(context_, asio::ip::udp::endpoint(asio::ip::udp::v4(), 13)) {
        startReceive();
    }

    void run() {
        context_.run();
    }

private:
    void startReceive() {
        socket_.async_receive_from(asio::buffer(recvBuffer_), remoteEndpoint_,
            std::bind(&UDPServer::handleReceive, this,
                asio::placeholders::error,
                asio::placeholders::bytes_transferred));
    }

    void handleReceive(const std::error_code &error, size_t) {
        if (!error) {
            auto message = std::make_shared<std::string>(makeDayTime());
            socket_.async_send_to(asio::buffer(*message), remoteEndpoint_,
                std::bind(&UDPServer::handleSend, this, message,
                    asio::placeholders::error,
                    asio::placeholders::bytes_transferred));
            startReceive();
        }
    }

    void handleSend(std::shared_ptr<std::string>, const std::error_code&, size_t) {}

private:
    asio::io_context context_;
    asio::ip::udp::socket socket_;
    asio::ip::udp::endpoint remoteEndpoint_;
    std::array<char, 1> recvBuffer_{};
};

int main() {
    UDPServer server;
    server.run();

    return 0;
}
