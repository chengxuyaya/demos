#include <any>
#include <array>
#include <memory>
#include <iostream>
#include <thread>
#include <cstdlib>

#include <uv.h>

void test(uv_work_t* req) {
    auto data = std::any_cast<std::shared_ptr<int>>(*reinterpret_cast<std::any*>(req->data));
    printf("%d\n", *data);
    std::this_thread::sleep_for(std::chrono::seconds(1));
}

void afterTest(uv_work_t* req, int status) {
    printf("after %d\n", *reinterpret_cast<int*>(req->data));
    delete reinterpret_cast<int*>(req->data);
    req->data = nullptr;
    delete req;
    req = nullptr;
}

int main() {
    for (int i = 0; i < 1024; i++) {
        uv_work_t* req = new uv_work_t();
        auto data = std::make_shared<int>(i);
        req->data = new std::any(data);
        uv_queue_work(uv_default_loop(), req, test, afterTest);
    }

    uv_run(uv_default_loop(), UV_RUN_DEFAULT);

    return 0;
}
