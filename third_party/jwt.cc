#include <iostream>
#include <string>

#include <jwt-cpp/jwt.h>

int main() {
    std::cout << "hello jwt" << std::endl;
    auto token = jwt::create()
                 .set_issuer("auth0")
                 .set_type("JWT")
                 .set_id("rsa-create-example")
                 .set_issued_at(std::chrono::system_clock::now())
                 .set_expires_at(std::chrono::system_clock::now() + std::chrono::seconds{36000})
                 .set_payload_claim("sample", jwt::claim(std::string{"test"}))
                 .sign(jwt::algorithm::hs256("test"));


    auto decode = jwt::decode(token);
    auto verify = jwt::verify().allow_algorithm(jwt::algorithm::hs256("test")).with_issuer("auth0");
    verify.verify(decode);

    std::cout << decode.get_header() << std::endl;
    std::cout << decode.get_payload() << std::endl;

    for (auto &e : decode.get_header_json()) {
        std::cout << e.first << " " << e.second << std::endl;
    }


    return 0;
}
