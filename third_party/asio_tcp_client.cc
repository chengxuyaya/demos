#include <iostream>

#include <asio/ip/tcp.hpp>
#include <asio/connect.hpp>

int main() {
    asio::io_context context;
    asio::ip::tcp::resolver resolver(context);
    auto endpoints = resolver.resolve("127.0.0.1", "daytime");
    asio::ip::tcp::socket socket(context);

    asio::connect(socket, endpoints);
    while (true) {
        char buf[128];
        std::error_code error;

        size_t len = socket.read_some(asio::buffer(buf), error);
        if (error == asio::error::eof) {
            break;
        } else if (error) {
            break;
        }
        std::cout << buf << std::endl;
    }

    return 0;
}
