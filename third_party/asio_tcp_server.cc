#include <ctime>
#include <iostream>
#include <memory>
#include <string>
#include <functional>

#include <asio/ip/tcp.hpp>
#include <asio/write.hpp>
#include <asio/placeholders.hpp>

std::string makeDayTime() {
    time_t now = time(0);
    return std::ctime(&now);
}

class TCPConnection : public std::enable_shared_from_this<TCPConnection> {
public:
    static std::shared_ptr<TCPConnection> create(asio::io_context& context) {
        return std::shared_ptr<TCPConnection>(new TCPConnection(context));
    }

    asio::ip::tcp::socket& socket() { return socket_; }

    void start() {
        message_ = makeDayTime();
        asio::async_write(socket_, asio::buffer(message_),
            std::bind(&TCPConnection::handleWrite, shared_from_this(),
                asio::placeholders::error, asio::placeholders::bytes_transferred));
    }

private:
    TCPConnection(asio::io_context& context) : socket_(context) {}

    void handleWrite(const std::error_code&, size_t) { std::cout << "handleWrite" << std::endl; }

private:
    asio::ip::tcp::socket socket_;
    std::string message_;
};

class TCPServer {
public:
    TCPServer()
        : context_(), acceptor_(context_, asio::ip::tcp::endpoint(asio::ip::tcp::v4(), 13)) {
        startAccept();
    }

    void run() {
        context_.run();
    }

private:
    void startAccept() {
        std::shared_ptr<TCPConnection> connection = TCPConnection::create(context_);
        acceptor_.async_accept(connection->socket(),
            std::bind(&TCPServer::handleAccept, this, connection, asio::placeholders::error));
    }

    void handleAccept(std::shared_ptr<TCPConnection> connection, const std::error_code& error) {
        std::cout << "accept" << std::endl;
        if (!error) {
            connection->start();
        }
        startAccept();
    }

private:
    asio::io_context context_;
    asio::ip::tcp::acceptor acceptor_;
};

int main() {
    TCPServer server;
    server.run();

    return 0;
}
