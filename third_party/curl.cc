#include <iostream>

#include <curl/curl.h>

int main() {
    std::cout << "hello libcurl" << std::endl;

    CURL *curl = nullptr;
    CURLcode errCode = CURLE_OK;

    curl = curl_easy_init();
    if (curl == nullptr) {
        std::cerr << "curl is nullptr" << std::endl;
        return -1;
    }

    curl_easy_setopt(curl, CURLOPT_URL, "127.0.0.1:3000/test");
    curl_easy_setopt(curl, CURLOPT_POSTFIELDS, "name=daniel&project=curl");
    curl_easy_setopt(curl, CURLOPT_POST, true);

    errCode = curl_easy_perform(curl);
    if (errCode != CURLE_OK) {
        std::cerr << "request error, errCode " << errCode << " errMsg " << curl_easy_strerror(errCode) << std::endl;
        return -1;
    }

    curl_easy_cleanup(curl);
    return 0;
}
