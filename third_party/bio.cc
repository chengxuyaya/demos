#include <iostream>
#include <string_view>
#include <memory>
#include <cstring>

#include <openssl/bio.h>
#include <openssl/evp.h>
#include <openssl/buffer.h>

int main() {
    constexpr std::string_view input = "hello base64";

    BIO *bio = nullptr;
    BIO *b64 = nullptr;
    BUF_MEM *bufferPtr;

    b64 = BIO_new(BIO_f_base64());
    bio = BIO_new(BIO_s_mem());
    bio = BIO_push(b64, bio);

    BIO_set_flags(bio, BIO_FLAGS_BASE64_NO_NL);

    BIO_write(bio, input.data(), input.size());
    BIO_flush(bio);
    BIO_get_mem_ptr(bio, &bufferPtr);

    std::string output(bufferPtr->data);
    printf("%lu\n", output.size());

    BIO_free_all(bio);

    printf("%s %lu\n", output.c_str(), output.size());

    bio = BIO_new_mem_buf(output.data(), output.size());
    b64 = BIO_new(BIO_f_base64());
    bio = BIO_push(b64, bio);

    BIO_set_flags(bio, BIO_FLAGS_BASE64_NO_NL);

    std::string result;
    std::unique_ptr<uint8_t[]> read = std::make_unique<uint8_t[]>(8);
    while (BIO_read(bio, read.get(), 8) != 0) {
        std::cout << "read " << read.get() << std::endl;
        result.append(reinterpret_cast<const char *>(read.get()));
        std::memset(read.get(), 0, 8);
    }
    BIO_free_all(bio);

    printf("%s %lu\n", result.c_str(), result.size());

    return 0;
}
