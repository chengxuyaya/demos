#include <iostream>
#include <string>
#include <limits>

#include <qrencode.h>
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include <stb_image_write.h>

int main() {
    std::cout << "hello qrencode" << std::endl;
    printf("%d\n", std::numeric_limits<uint8_t>::max());
    std::string text = "hello, QR code";

    QRcode *qrcode = QRcode_encodeString(text.c_str(), 0, QR_ECLEVEL_L, QR_MODE_8, 1);

    uint8_t *imageData = (uint8_t *)malloc(qrcode->width * qrcode->width * 4);

    for (int y = 0; y < qrcode->width; y++) {
        for (int x = 0; x < qrcode->width; x++) {
            uint8_t color = qrcode->data[y * qrcode->width + x] & 1 ? 0 : 255;
            imageData[(y * qrcode->width + x) * 4 + 0] = color;
            imageData[(y * qrcode->width + x) * 4 + 1] = color;
            imageData[(y * qrcode->width + x) * 4 + 2] = color;
            imageData[(y * qrcode->width + x) * 4 + 3] = 255;
        }
    }

    stbi_write_png("qrcode.png", qrcode->width, qrcode->width, 4, imageData, qrcode->width * 4);

    QRcode_free(qrcode);
    free(imageData);

    return 0;
}
