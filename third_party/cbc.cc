#include <openssl/evp.h>
#include <openssl/aes.h>
#include <iostream>
#include <string>
#include <sstream>

void handleErrors() {
    std::cout << "error" << std::endl;
}

// AES加密函数
std::string encryptAES(const std::string& plaintext, const std::string& key, const std::string& iv) {
    EVP_CIPHER_CTX* ctx;
    int len;
    int ciphertext_len;
    unsigned char ciphertext[4352];

    // 创建并初始化上下文
    if (!(ctx = EVP_CIPHER_CTX_new())) handleErrors();

    // 初始化加密操作
    if (1 != EVP_EncryptInit_ex(ctx, EVP_aes_128_cbc(), NULL, (const unsigned char*)key.c_str(),
                                (const unsigned char*)iv.c_str()))
        handleErrors();

    // 提供要加密的消息，并获取加密输出
    if (1 != EVP_EncryptUpdate(ctx, ciphertext, &len, (const unsigned char*)plaintext.c_str(), plaintext.length()))
        handleErrors();
    ciphertext_len = len;
    std::cout << "len: " << len << std::endl;

    // 结束加密
    if (1 != EVP_EncryptFinal_ex(ctx, ciphertext + len, &len)) handleErrors();
    ciphertext_len += len;

    // 清理上下文
    EVP_CIPHER_CTX_free(ctx);

    // 返回加密后的密文
    std::stringstream ss;
    printf("%s\n", ciphertext);
    for (int i = 0; i < ciphertext_len; i++) {
        ss << ciphertext[i];
    }
    return ss.str();
}

// AES解密函数
std::string decryptAES(const std::string& ciphertext, const std::string& key, const std::string& iv) {
    EVP_CIPHER_CTX* ctx;
    int len;
    int plaintext_len;
    unsigned char plaintext[4352];

    // 创建并初始化上下文
    if (!(ctx = EVP_CIPHER_CTX_new())) handleErrors();

    // 初始化解密操作
    if (1 != EVP_DecryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, (const unsigned char*)key.c_str(),
                                (const unsigned char*)iv.c_str()))
        handleErrors();

    // 提供要解密的密文，并获取解密输出
    if (1 != EVP_DecryptUpdate(ctx, plaintext, &len, (const unsigned char*)ciphertext.c_str(), ciphertext.length()))
        handleErrors();
    plaintext_len = len;

    // 结束解密
    if (1 != EVP_DecryptFinal_ex(ctx, plaintext + len, &len)) handleErrors();
    plaintext_len += len;

    // 清理上下文
    EVP_CIPHER_CTX_free(ctx);

    // 返回解密后的明文
    return std::string((char*)plaintext, plaintext_len);
}

int main() {
    std::string key = "01234567890123456789012345678901";  // 256-bit key
    std::string iv = "0123456789012345";                   // 128-bit IV
    std::string plaintext = "The quick brown fox jumps over the lazy dog";
    // for (int i = 0; i < 100; i++) {
    //     plaintext.append("The quick brown fox jumps over the lazy dog");
    // }

    // 加密
    std::string ciphertext = encryptAES(plaintext, key, iv);
    // std::cout << "Ciphertext: " << ciphertext << " size: " << ciphertext.size() << std::endl;

    // 解密
    // std::string decryptedtext = decryptAES(ciphertext, key, iv);
    // std::cout << "Decrypted text: " << decryptedtext << " size: " << decryptedtext.size() << std::endl;

    AES_KEY aesKey;
    uint8_t chiphertext[4352];
    AES_set_encrypt_key(reinterpret_cast<const uint8_t*>(key.c_str()), 128, &aesKey);
    AES_cbc_encrypt(reinterpret_cast<const uint8_t*>(plaintext.c_str()), chiphertext, plaintext.size(), &aesKey,
        reinterpret_cast<uint8_t*>(iv.data()), AES_ENCRYPT);
    printf("%s\n", chiphertext);

    return 0;
}
