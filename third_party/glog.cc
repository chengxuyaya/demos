#include <iostream>
#include <limits>

#include <glog/logging.h>
#include <glog/raw_logging.h>

int main() {
    google::InitGoogleLogging("glogdemo");
    google::SetStderrLogging(google::INFO);
    google::SetLogDestination(google::INFO, "./out/var/log");
    google::SetLogFilenameExtension(".log");
    google::InstallFailureSignalHandler();
    FLAGS_colorlogtostdout = true;
    FLAGS_logbufsecs = 0;

    LOGI << "hello glog";
    RAW_LOG(INFO, "hello world");

    google::ShutdownGoogleLogging();
    return 0;
}
