/*
 * Copyright (c) 2023 chengxuya.
 * This is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *         http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

#pragma once

#include <string>

#include <glm/glm.hpp>

class Shader {
public:
    Shader(const std::string &vertexPath, const std::string &fragmentPath);
    ~Shader();

    bool CompileShader();
    void Use() const;
    const uint32_t GetProgramID() const;

    void SetInt(const std::string &name, int32_t value) const;
    void SetUInt(const std::string &name, uint32_t value) const;
    void SetFloat(const std::string &name, float value) const;
    void SetBool(const std::string &name, bool value) const;
    void SetVec3(const std::string &name, const glm::vec3 &value) const;
    void SetVec3(const std::string &name, float x, float y, float z) const;
    void SetVec4(const std::string &name, const glm::vec4 &value) const;
    void SetVec4(const std::string &name, float x, float y, float z, float w) const;
    void SetMat4(const std::string &name, const glm::mat4 &mat) const;

private:
    uint32_t programID_;
    char *vertexPath_;
    char *fragmentPath_;

private:
    int32_t GetUniformLocation(const std::string &name) const;
};
