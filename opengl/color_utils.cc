/*
 * Copyright (c) 2023 chengxuya.
 * This is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *         http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

#include "color_utils.h"

namespace {

constexpr float originColor = 1.0 / 255;

inline int32_t HexToInt(const std::string &value) {
    return std::stoi(value, nullptr, 16);
}

}  // namespace

std::array<float, 3> HexToRGB(const std::string &color) {
    bool haveSharp = (color[0] == '#');
    std::string r = haveSharp ? color.substr(1, 2) : color.substr(0, 2);
    std::string g = haveSharp ? color.substr(3, 2) : color.substr(2, 2);
    std::string b = haveSharp ? color.substr(5, 2) : color.substr(4, 2);
    return GetRGB(HexToInt(r), HexToInt(g), HexToInt(b));
}

std::array<float, 3> GetRGB(int32_t r, int32_t g, int32_t b) {
    return {originColor * r, originColor * g, originColor * b};
}
