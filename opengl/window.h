/*
 * Copyright (c) 2023 chengxuya.
 * This is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *         http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

#include <cstdint>
#include <functional>
#include <string>
#include <utility>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

using DrawFunc = std::function<void()>;
using InputFunc = std::function<void(GLFWwindow *window)>;
using CursorFunc = std::function<void(GLFWwindow *window, double x, double y)>;
using ScrollFunc = std::function<void(GLFWwindow *window, double xoffset, double yoffset)>;

class Window {
public:
    Window(int32_t fps);
    Window(int32_t width, int32_t height);
    Window(int32_t width, int32_t height, int32_t fps);
    Window(const std::string &name = DEFAULT_NAME, int32_t width = DEFAULT_WIDTH, int32_t height = DEFAULT_HEIGHT,
           int32_t fps = DEFAULT_FPS);
    ~Window();

    bool Init();
    void Run() const;
    void SetDrawFunc(const DrawFunc &drawFunc);
    void SetInputFunc(const InputFunc &inputFunc);
    void SetCursorFunc(const CursorFunc &cursorFunc);
    void SetScrollFunc(const ScrollFunc &scrollFunc);
    std::pair<int32_t, int32_t> GetExtent() const;

    friend void WindowFramebufferSizeCallback(GLFWwindow *window, int32_t width, int32_t height);
    friend void WindowCursorPosCallback(GLFWwindow *window, double x, double y);
    friend void WindowScrollCallback(GLFWwindow *window, double xoffset, double yoffset);

private:
    std::string name_;
    int32_t width_;
    int32_t height_;
    int32_t fps_;  // frame per second.
    GLFWwindow *window_;

    DrawFunc drawFunc_;
    InputFunc inputFunc_;
    CursorFunc cursorFunc_;
    ScrollFunc scrollFunc_;

private:
    static constexpr char DEFAULT_NAME[] = "window";
    static constexpr int32_t DEFAULT_WIDTH = 800;
    static constexpr int32_t DEFAULT_HEIGHT = 600;
    static constexpr int32_t DEFAULT_FPS = 30;

private:
    void ProcessExit() const;
    void Draw() const;
};
