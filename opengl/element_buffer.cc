/*
 * Copyright (c) 2023 chengxuya.
 * This is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *         http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

#include "element_buffer.h"

#include <glad/glad.h>
#include <GLFW/glfw3.h>

ElementBuffer::ElementBuffer(const std::vector<uint32_t> &indices) {
    indices_ = new std::vector<uint32_t>();
    *indices_ = indices;
}

ElementBuffer::~ElementBuffer() {
    if (indices_ != nullptr) {
        delete indices_;
    }
}

void ElementBuffer::GenBuffer() {
    glGenBuffers(1, &EBO_);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO_);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices_->size() * sizeof(uint32_t), indices_->data(), GL_STATIC_DRAW);
    delete indices_;  // TODO a bug.
    indices_ = nullptr;
}
