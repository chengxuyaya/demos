/*
 * Copyright (c) 2023 chengxuya.
 * This is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *         http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
#include <cstdint>
#include <utility>

#include <glm/glm.hpp>

class Camera {
public:
    enum Direction {
        FORWARD,
        BACKWARD,
        LEFT,
        RIGHT,
    };

public:
    Camera(std::pair<int32_t, int32_t> extent);
    ~Camera() = default;

    const glm::mat4 &LookAt() const;
    float GetFOV() const;
    void Move(Direction direction);
    void Rotate(double x, double y);
    void Zoom(double yoffset);

private:
    // TODO member variables need to optimize.
    glm::vec3 position_;
    glm::vec3 front_;
    glm::vec3 up_;
    double yaw_ = -90.0f;
    double pitch_ = 0.0f;
    double lastX_;
    double lastY_;
    bool isFirst_;

    glm::mat4 lookAt_;
    double fov_;

private:
    void Update();
};
