/*
 * Copyright (c) 2023 chengxuya.
 * This is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *         http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

#pragma once

#include <cstddef>
#include <cstdint>
#include <vector>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

class VertexBuffer {
public:
    VertexBuffer(const std::vector<float> &vertices, GLenum targetType = GL_ARRAY_BUFFER,
                 GLenum usage = GL_STATIC_DRAW);
    ~VertexBuffer();

    void GenBuffer(uint32_t index, std::size_t vertexNum, int32_t stride, int32_t offset);
    void Enable(uint32_t index) const;
    void Disable(uint32_t index) const;

private:
    uint32_t VBO_;
    GLenum targetType_;
    GLenum usage_;
    std::vector<float> *vertices_;
};
