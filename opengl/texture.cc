/*
 * Copyright (c) 2023 chengxuya.
 * This is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *         http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

#include "texture.h"

#include <unordered_map>

#include <stb_image.h>

namespace {

const std::unordered_map<GLenum, int32_t> TEXTURE_UNIT = {
    {GL_TEXTURE0, 0},   {GL_TEXTURE1, 1},   {GL_TEXTURE2, 2},   {GL_TEXTURE3, 3},   {GL_TEXTURE4, 4},
    {GL_TEXTURE5, 5},   {GL_TEXTURE6, 6},   {GL_TEXTURE7, 7},   {GL_TEXTURE8, 8},   {GL_TEXTURE9, 9},
    {GL_TEXTURE10, 10}, {GL_TEXTURE11, 11}, {GL_TEXTURE12, 12}, {GL_TEXTURE13, 13}, {GL_TEXTURE14, 14},
    {GL_TEXTURE15, 15}, {GL_TEXTURE16, 16}, {GL_TEXTURE17, 17}, {GL_TEXTURE18, 18}, {GL_TEXTURE19, 19},
    {GL_TEXTURE20, 20}, {GL_TEXTURE21, 21}, {GL_TEXTURE22, 22}, {GL_TEXTURE23, 23}, {GL_TEXTURE24, 24},
    {GL_TEXTURE25, 25}, {GL_TEXTURE26, 26}, {GL_TEXTURE27, 27}, {GL_TEXTURE28, 28}, {GL_TEXTURE29, 29},
    {GL_TEXTURE30, 30}, {GL_TEXTURE31, 31},
};

inline GLenum TransformComponent(int32_t component) {
    switch (component) {
        case STBI_rgb:
        case STBI_grey:
            return GL_RGB;
        case STBI_rgb_alpha:
        case STBI_grey_alpha:
            return GL_RGBA;
        default:
            return STBI_default;
    }
}

}  // namespace

Texture::Texture(GLenum textureUnit, const std::string &image)
    : texture_(0), textureUnit_(std::make_pair(textureUnit, TEXTURE_UNIT.find(textureUnit)->second)), data_(nullptr) {
    image_ = new std::string();
    *image_ = image;
}

Texture::~Texture() {
    if (image_ != nullptr) {
        delete image_;
    }
}

void Texture::GenTexture() {
    glGenTextures(1, &texture_);
    glActiveTexture(std::get<GLenum>(textureUnit_));
    glBindTexture(GL_TEXTURE_2D, texture_);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    auto [success, width, height, component] = LoadImage();
    if (!success) {
        return;
    }
    glTexImage2D(GL_TEXTURE_2D, 0, component, width, height, 0, component, GL_UNSIGNED_BYTE, data_);
    stbi_image_free(data_);

    glGenerateMipmap(GL_TEXTURE_2D);
}

void Texture::Bind() const {
    glBindTexture(GL_TEXTURE_2D, texture_);
}

void Texture::Active() const {
    glActiveTexture(std::get<GLenum>(textureUnit_));
}

int32_t Texture::GetUnit() const {
    return std::get<int32_t>(textureUnit_);
}

std::tuple<bool, int32_t, int32_t, GLenum> Texture::LoadImage() {
    int32_t width;
    int32_t height;
    int32_t component;

    data_ = stbi_load(image_->c_str(), &width, &height, &component, 0);
    if (data_ == nullptr) {
        return std::make_tuple(false, 0, 0, 0);
    }
    return std::make_tuple(true, width, height, TransformComponent(component));
}
