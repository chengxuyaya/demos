/*
 * Copyright (c) 2023 chengxuya.
 * This is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *         http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

#include "shader.h"

#include <array>
#include <cstring>
#include <sstream>
#include <fstream>

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <glm/gtc/type_ptr.hpp>

namespace {

enum class CheckType {
    VERTEX,
    FRAGMENT,
    PROGRAME,
};

bool CheckCompile(uint32_t shader, CheckType type) {
    int success;
    constexpr int LOG_SIZE = 1023;
    std::array<char, LOG_SIZE> infoLog;

    if (type == CheckType::PROGRAME) {
        glGetProgramiv(shader, GL_LINK_STATUS, &success);
        if (!success) {
            glGetProgramInfoLog(shader, LOG_SIZE, nullptr, infoLog.data());
            return false;
        }
    } else {
        glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
        if (!success) {
            glGetShaderInfoLog(shader, LOG_SIZE, nullptr, infoLog.data());
            return false;
        }
    }
    return true;
}

std::string Read(const std::string &file) {
    std::ifstream ifs;
    ifs.exceptions(std::ifstream::failbit | std::ifstream::badbit);
    std::string result;
    try {
        ifs.open(file);
        std::stringstream ss;
        ss << ifs.rdbuf();
        result = ss.str();
    } catch (std::ifstream::failure &error) {
        return {};
    }
    ifs.close();
    return result;
}

}  // namespace

Shader::Shader(const std::string &vertexPath, const std::string &fragmentPath)
    : programID_(0), vertexPath_(new char[vertexPath.size() + 1]), fragmentPath_(new char[fragmentPath.size() + 1]) {
    std::strcpy(vertexPath_, vertexPath.data());
    std::strcpy(fragmentPath_, fragmentPath.data());
}

Shader::~Shader() {
    if (vertexPath_ != nullptr) {
        delete[] vertexPath_;
    }
    if (fragmentPath_ != nullptr) {
        delete[] fragmentPath_;
    }
}

bool Shader::CompileShader() {
    std::string vertexFile = Read(vertexPath_).c_str();
    const char *vertexCode = vertexFile.c_str();
    uint32_t vertexShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertexShader, 1, &vertexCode, nullptr);
    glCompileShader(vertexShader);
    if (!CheckCompile(vertexShader, CheckType::VERTEX)) {
        return false;
    }

    std::string fragmentFile = Read(fragmentPath_).c_str();
    const char *fragmentCode = fragmentFile.c_str();
    uint32_t fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragmentShader, 1, &fragmentCode, nullptr);
    glCompileShader(fragmentShader);
    if (!CheckCompile(fragmentShader, CheckType::FRAGMENT)) {
        return false;
    }

    programID_ = glCreateProgram();
    glAttachShader(programID_, vertexShader);
    glAttachShader(programID_, fragmentShader);
    glLinkProgram(programID_);
    if (!CheckCompile(programID_, CheckType::PROGRAME)) {
        return false;
    }

    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);
    delete[] vertexPath_;
    vertexPath_ = nullptr;
    delete[] fragmentPath_;
    fragmentPath_ = nullptr;
    return true;
}

void Shader::Use() const {
    glUseProgram(programID_);
}

const uint32_t Shader::GetProgramID() const {
    return programID_;
}

void Shader::SetInt(const std::string &name, int32_t value) const {
    int32_t location = GetUniformLocation(name);
    if (location == -1) {
        return;
    }
    glUniform1i(location, value);
}

void Shader::SetUInt(const std::string &name, uint32_t value) const {
    int32_t location = GetUniformLocation(name);
    if (location == -1) {
        return;
    }
    glUniform1ui(location, value);
}

void Shader::SetFloat(const std::string &name, float value) const {
    int32_t location = GetUniformLocation(name);
    if (location == -1) {
        return;
    }
    glUniform1f(location, value);
}

void Shader::SetBool(const std::string &name, bool value) const {
    int32_t location = GetUniformLocation(name);
    if (location == -1) {
        return;
    }
    glUniform1i(location, static_cast<int32_t>(value));
}

void Shader::SetVec3(const std::string &name, const glm::vec3 &value) const {
    int32_t location = GetUniformLocation(name);
    if (location == -1) {
        return;
    }
    glUniform3fv(location, 1, glm::value_ptr(value));
}

void Shader::SetVec3(const std::string &name, float x, float y, float z) const {
    int32_t location = GetUniformLocation(name);
    if (location == -1) {
        return;
    }
    glUniform3f(location, x, y, z);
}

void Shader::SetVec4(const std::string &name, const glm::vec4 &value) const {
    int32_t location = GetUniformLocation(name);
    if (location == -1) {
        return;
    }
    glUniform4fv(location, 1, glm::value_ptr(value));
}

void Shader::SetVec4(const std::string &name, float x, float y, float z, float w) const {
    int32_t location = GetUniformLocation(name);
    if (location == -1) {
        return;
    }
    glUniform4f(location, x, y, z, w);
}

void Shader::SetMat4(const std::string &name, const glm::mat4 &mat) const {
    int32_t location = GetUniformLocation(name);
    if (location == -1) {
        return;
    }
    glUniformMatrix4fv(location, 1, GL_FALSE, glm::value_ptr(mat));
}

int32_t inline Shader::GetUniformLocation(const std::string &name) const {
    return glGetUniformLocation(programID_, name.c_str());
}
