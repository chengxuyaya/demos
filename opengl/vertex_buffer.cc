/*
 * Copyright (c) 2023 chengxuya.
 * This is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *         http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

#include "vertex_buffer.h"

VertexBuffer::VertexBuffer(const std::vector<float> &vertices, GLenum targetType, GLenum usage)
    : VBO_(0), targetType_(targetType), usage_(usage) {
    vertices_ = new std::vector<float>();  // TODO use pointer is current?
    *vertices_ = vertices;
}

VertexBuffer::~VertexBuffer() {
    if (vertices_ != nullptr) {
        delete vertices_;
    }
}

void VertexBuffer::GenBuffer(uint32_t index, std::size_t vertexNum, int32_t stride, int32_t offset) {
    glGenBuffers(1, &VBO_);
    glBindBuffer(targetType_, VBO_);
    glBufferData(targetType_, vertices_->size() * sizeof(float), vertices_->data(), usage_);
    glVertexAttribPointer(index, vertexNum, GL_FLOAT, GL_FALSE, stride * sizeof(float),
                          reinterpret_cast<void *>(offset * sizeof(float)));
}
void VertexBuffer::Enable(uint32_t index) const {
    glEnableVertexAttribArray(index);
}

void VertexBuffer::Disable(uint32_t index) const {
    glDisableVertexAttribArray(index);
}
