/*
 * Copyright (c) 2023 chengxuya.
 * This is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *         http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

#include "window.h"

#include <unordered_map>

#include <glog/logging.h>

namespace {

std::unordered_map<GLFWwindow *, Window *> windows;

inline Window *GetCurrentWindow(GLFWwindow *window) {
    auto win = windows.find(window);
    if (win == windows.end()) {
        LOGW << "not found window";
        return nullptr;
    }
    return win->second;
}

}  // namespace

void WindowFramebufferSizeCallback(GLFWwindow *window, int32_t width, int32_t height) {
    Window *win = GetCurrentWindow(window);
    if (win == nullptr) {
        return;
    }
    glViewport(0, 0, width, height);
    win->width_ = width;
    win->height_ = height;
}

void WindowCursorPosCallback(GLFWwindow *window, double x, double y) {
    Window *win = GetCurrentWindow(window);
    if (win == nullptr) {
        return;
    }
    if (win->cursorFunc_ != nullptr) {
        win->cursorFunc_(window, x, y);
    }
}

void WindowScrollCallback(GLFWwindow *window, double xoffset, double yoffset) {
    Window *win = GetCurrentWindow(window);
    if (win == nullptr) {
        return;
    }
    if (win->scrollFunc_ != nullptr) {
        win->scrollFunc_(window, xoffset, yoffset);
    }
}

Window::Window(int32_t fps) : Window(DEFAULT_NAME, DEFAULT_WIDTH, DEFAULT_HEIGHT, fps) {
}

Window::Window(int32_t width, int32_t height) : Window(DEFAULT_NAME, width, height, DEFAULT_FPS) {
}

Window::Window(int32_t width, int32_t height, int32_t fps) : Window(DEFAULT_NAME, width, height, fps) {
}

Window::Window(const std::string &name, int32_t width, int32_t height, int32_t fps)
    : name_(name), width_(width), height_(height), fps_(fps), window_(nullptr), drawFunc_(nullptr) {
}

Window::~Window() {
    windows.erase(window_);
    glfwTerminate();
}

bool Window::Init() {
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif

    window_ = glfwCreateWindow(width_, height_, name_.c_str(), nullptr, nullptr);
    if (window_ == nullptr) {
        LOGE << "window is nullptr";
        return false;
    }
    windows.emplace(window_, this);

    glfwMakeContextCurrent(window_);
    glfwSetFramebufferSizeCallback(window_, WindowFramebufferSizeCallback);
    glfwSetInputMode(window_, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

    if (!gladLoadGLLoader(reinterpret_cast<GLADloadproc>(glfwGetProcAddress))) {
        LOGE << "init glad failed";
        return false;
    }
    glEnable(GL_DEPTH_TEST);

    return true;
}

void Window::Run() const {
    float lastTime = 0.0;
    float averageTime = 1.0 / fps_;
    while (!glfwWindowShouldClose(window_)) {
        ProcessExit();
        float currentTime = glfwGetTime();
        if (currentTime >= (lastTime + averageTime)) {
            Draw();
            lastTime = currentTime;
        }
    }
}

void Window::SetDrawFunc(const DrawFunc &drawFunc) {
    drawFunc_ = drawFunc;
}

void Window::SetInputFunc(const InputFunc &inputFunc) {
    inputFunc_ = inputFunc;
}

void Window::SetCursorFunc(const CursorFunc &cursorFunc) {
    cursorFunc_ = cursorFunc;
    glfwSetCursorPosCallback(window_, WindowCursorPosCallback);
}

void Window::SetScrollFunc(const ScrollFunc &scrollFunc) {
    scrollFunc_ = scrollFunc;
    glfwSetScrollCallback(window_, WindowScrollCallback);
}

std::pair<int32_t, int32_t> Window::GetExtent() const {
    return std::make_pair(width_, height_);
}

inline void Window::ProcessExit() const {
    if (glfwGetKey(window_, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
        LOGW << "exit";
        glfwSetWindowShouldClose(window_, true);
    }
}
inline void Window::Draw() const {
    if (drawFunc_ != nullptr) {
        drawFunc_();
    }
    if (inputFunc_ != nullptr) {
        inputFunc_(window_);
    }

    glfwSwapBuffers(window_);
    glfwPollEvents();
}
