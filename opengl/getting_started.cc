/*
 * Copyright (c) 2023 chengxuya.
 * This is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *         http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

#include <array>
#include <memory>
#include <vector>
#include <iostream>

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "camera.h"
#include "color_utils.h"
#include "element_buffer.h"
#include "shader.h"
#include "texture.h"
#include "vertex_array.h"
#include "vertex_buffer.h"
#include "window.h"

/*
 * 顶点数组对象：Vertex Array Object, VAO.
 * 顶点缓冲对象：Vertex Buffer Object, VBO.
 * 元素缓冲对象：Element Buffer Object, EBO. or 索引缓冲对象：Index Buffer Object, IBO.
 *
 * 纹理贴图坐标系(UV坐标系)：
 * u轴：横向坐标，对应于纹理的宽度方向
 *   u = 0 表示纹理的最左边
 *   u = 1 表示纹理的最右边
 * v轴：纵向坐标，对应于纹理的高度方向
 *   v = 0 表示纹理的最上边
 *   v = 1 表示纹理的最下边
 *
 *  (0,0) -----u----- (1,0)
 *    |                 |
 *    |                 |
 *    v                 v
 *    |                 |
 *    |                 |
 *  (0,1) -----u----- (1,1)
 *
 *
 * 标准化设备坐标(Normalized Device Coordinate, NDC):
 * 顶点的x, y, z坐标的范围-1.0-1.0
 *
 *  (-0.5,-0.5) ----------- (+0.5,+0.5)
 *       |                       |
 *       |                       |
 *       |           +           |
 *       |                       |
 *       |                       |
 *  (-0.5,+0.5) ----------- (+0.5,-0.5)
 *
 *
 * 坐标系统：
 * 局部空间(LocalSpace) or 物体空间(ObjectSpace) -> 世界空间(WorldSpace)->
 * 观察空间(ViewSpace) or 世界空间(ViewSpace) -> 裁剪空间(ClipSpace) -> 屏幕空间(ScreenSpace)
 *
 * mode -> view -> projection
 *
 *
 * 变换顺序:
 * 位移 > 旋转 > 缩放
 */

namespace {

const std::vector<float> cube = {
    -0.5, 0.5,  0.5,  1.0, 1.0, 1.0, 0.0, 0.0,  // 0
    0.5,  0.5,  0.5,  1.0, 1.0, 1.0, 1.0, 0.0,  // 1
    0.5,  -0.5, 0.5,  1.0, 1.0, 1.0, 1.0, 1.0,  // 2
    -0.5, -0.5, 0.5,  1.0, 1.0, 1.0, 0.0, 1.0,  // 3

    -0.5, 0.5,  -0.5, 1.0, 1.0, 1.0, 0.0, 0.0,  // 4
    -0.5, 0.5,  0.5,  1.0, 1.0, 1.0, 1.0, 0.0,  // 5
    -0.5, -0.5, 0.5,  1.0, 1.0, 1.0, 1.0, 1.0,  // 6
    -0.5, -0.5, -0.5, 1.0, 1.0, 1.0, 0.0, 1.0,  // 7

    0.5,  0.5,  -0.5, 1.0, 1.0, 1.0, 0.0, 0.0,  // 8
    -0.5, 0.5,  -0.5, 1.0, 1.0, 1.0, 1.0, 0.0,  // 9
    -0.5, -0.5, -0.5, 1.0, 1.0, 1.0, 1.0, 1.0,  // 10
    0.5,  -0.5, -0.5, 1.0, 1.0, 1.0, 0.0, 1.0,  // 11

    0.5,  0.5,  0.5,  1.0, 1.0, 1.0, 0.0, 0.0,  // 12
    0.5,  0.5,  -0.5, 1.0, 1.0, 1.0, 1.0, 0.0,  // 13
    0.5,  -0.5, -0.5, 1.0, 1.0, 1.0, 1.0, 1.0,  // 14
    0.5,  -0.5, 0.5,  1.0, 1.0, 1.0, 0.0, 1.0,  // 15

    -0.5, 0.5,  -0.5, 1.0, 1.0, 1.0, 0.0, 0.0,  // 16
    0.5,  0.5,  -0.5, 1.0, 1.0, 1.0, 1.0, 0.0,  // 17
    0.5,  0.5,  0.5,  1.0, 1.0, 1.0, 1.0, 1.0,  // 18
    -0.5, 0.5,  0.5,  1.0, 1.0, 1.0, 0.0, 1.0,  // 19

    -0.5, -0.5, 0.5,  1.0, 1.0, 1.0, 0.0, 0.0,  // 20
    0.5,  -0.5, 0.5,  1.0, 1.0, 1.0, 1.0, 0.0,  // 21
    0.5,  -0.5, -0.5, 1.0, 1.0, 1.0, 1.0, 1.0,  // 22
    -0.5, -0.5, -0.5, 1.0, 1.0, 1.0, 0.0, 1.0   // 23
};

const std::vector<uint32_t> cubeElements = {0,  1,  2,  0,  2,  3,

                                            4,  5,  6,  4,  6,  7,

                                            8,  9,  10, 8,  10, 11,

                                            12, 13, 14, 12, 14, 15,

                                            16, 17, 18, 16, 18, 19,

                                            20, 21, 22, 20, 22, 23};

}  // namespace

int main() {
    std::cout << "hello opengl" << std::endl;

    std::shared_ptr<Window> window = std::make_shared<Window>(60);
    if (!window->Init()) {
        std::cout << "window init failed" << std::endl;
        return -1;
    }

    auto shader = std::make_shared<Shader>("out/share/opengl/vertex.glsl", "out/share/opengl/fragment.glsl");
    if (!shader->CompileShader()) {
        std::cout << "compile shader failed" << std::endl;
        return -1;
    }

    std::shared_ptr<VertexArray> VAO = std::make_shared<VertexArray>();
    VAO->Bind();

    std::unique_ptr<VertexBuffer> VBO = std::make_unique<VertexBuffer>(cube);
    VBO->GenBuffer(0, 3, 8, 0);
    VBO->Enable(0);
    VBO->GenBuffer(1, 3, 8, 3);
    VBO->Enable(1);
    VBO->GenBuffer(2, 2, 8, 6);
    VBO->Enable(2);

    std::unique_ptr<ElementBuffer> EBO = std::make_unique<ElementBuffer>(cubeElements);
    EBO->GenBuffer();
    VAO->UnBind();

    std::unique_ptr<Texture> wood = std::make_unique<Texture>(GL_TEXTURE0, "out/share/opengl/container.jpg");
    wood->GenTexture();
    std::unique_ptr<Texture> face = std::make_unique<Texture>(GL_TEXTURE1, "out/share/opengl/awesomeface.png");
    face->GenTexture();

    shader->Use();
    shader->SetInt("texture0", wood->GetUnit());
    shader->SetInt("texture1", face->GetUnit());

    VAO->Bind();
    std::array<float, 3> color = GetRGB(33, 33, 33);
    std::shared_ptr<Camera> camera = std::make_shared<Camera>(window->GetExtent());

    window->SetDrawFunc([shader, &color, window, camera]() {
        glClearColor(color.at(R), color.at(G), color.at(B), 1.0);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glm::mat4 model = glm::mat4(1.0f);
        float angle = glm::sin(glfwGetTime());
        model = glm::rotate(model, angle, glm::vec3(1.0f, 1.0f, 1.0f));
        glm::mat4 view = camera->LookAt();
        glm::mat4 projection = glm::mat4(1.0f);
        auto [width, height] = window->GetExtent();
        projection = glm::perspective(glm::radians(camera->GetFOV()),
                                      static_cast<float>(width) / static_cast<float>(height), 0.1f, 100.0f);

        shader->SetMat4("model", model);
        shader->SetMat4("view", view);
        shader->SetMat4("projection", projection);

        glDrawElements(GL_TRIANGLES, cubeElements.size(), GL_UNSIGNED_INT, 0);
    });
    window->SetInputFunc([camera](const auto window) {
        if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS || glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
            camera->Move(Camera::FORWARD);
        }
        if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS || glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
            camera->Move(Camera::BACKWARD);
        }
        if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS || glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
            camera->Move(Camera::LEFT);
        }
        if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS || glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {
            camera->Move(Camera::RIGHT);
        }
    });
    window->SetCursorFunc([camera](GLFWwindow *window, double x, double y) { camera->Rotate(x, y); });
    window->SetScrollFunc([camera](GLFWwindow *window, double xoffset, double yoffset) { camera->Zoom(yoffset); });
    window->Run();

    return 0;
}
