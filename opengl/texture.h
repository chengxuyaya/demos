/*
 * Copyright (c) 2023 chengxuya.
 * This is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *         http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

#include <cstdint>
#include <string>
#include <tuple>
#include <utility>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

class Texture {
public:
    Texture(GLenum textureUnit, const std::string &image);
    ~Texture();

    void GenTexture();
    void Bind() const;
    void Active() const;
    int32_t GetUnit() const;

private:
    uint32_t texture_;
    std::pair<GLenum, int32_t> textureUnit_;
    std::string *image_;
    uint8_t *data_;

    std::tuple<bool, int32_t, int32_t, GLenum> LoadImage();
};
