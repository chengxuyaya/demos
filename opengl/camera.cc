/*
 * Copyright (c) 2023 chengxuya.
 * This is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *         http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 *
 * Euler Angle 欧拉角
 * pitch 俯仰角
 * yaw 偏行角
 * fov Field of view 视野
 */

#include "camera.h"

#include <glm/gtc/matrix_transform.hpp>
#include <glog/logging.h>

Camera::Camera(std::pair<int32_t, int32_t> extent)
    : position_(glm::vec3(0.0f, 0.0f, 4.0f)),
      front_(glm::vec3(0.0f, 0.0f, -1.0f)),
      up_(glm::vec3(0.0f, 1.0f, 0.0f)),
      yaw_(-90.0f),
      pitch_(0.0f),
      lastX_(extent.first / 2),
      lastY_(extent.second / 2),
      isFirst_(true),
      fov_(45.0f) {
    Update();
}

const glm::mat4 &Camera::LookAt() const {
    return lookAt_;
}

float Camera::GetFOV() const {
    return fov_;
}

void Camera::Move(Direction direction) {
    float speed = 0.05f;
    switch (direction) {
        case FORWARD:
            position_ += front_ * speed;
            break;
        case BACKWARD:
            position_ -= front_ * speed;
            break;
        case LEFT:
            position_ -= glm::normalize(glm::cross(front_, up_)) * speed;
            break;
        case RIGHT:
            position_ += glm::normalize(glm::cross(front_, up_)) * speed;
            break;
        default:
            LOGW << "not match any direction";
    }
    Update();
}

void Camera::Rotate(double x, double y) {
    // TODO need to optimize.
    if (isFirst_) {
        lastX_ = x;
        lastY_ = y;
        isFirst_ = false;
    }
    double sensitivity = 0.05;
    double xoffset = x - lastX_;
    double yoffset = lastY_ - y;
    lastX_ = x;
    lastY_ = y;

    xoffset *= sensitivity;
    yoffset *= sensitivity;

    yaw_ += xoffset;
    pitch_ += yoffset;

    if (pitch_ > 89.0f) {
        pitch_ = 89.0f;
    }
    if (pitch_ < -89.0f) {
        pitch_ = -89.0f;
    }

    glm::vec3 front;
    front.x = glm::cos(glm::radians(yaw_)) * glm::cos(glm::radians(pitch_));
    front.y = glm::sin(glm::radians(pitch_));
    front.z = glm::sin(glm::radians(yaw_)) * glm::cos(glm::radians(pitch_));
    front_ = glm::normalize(front);
    Update();
}

void Camera::Zoom(double yoffset) {
    fov_ -= yoffset;
    if (fov_ < 1.0f) {
        fov_ = 1.0f;
    }
    if (fov_ > 45.0f) {
        fov_ = 45.0f;
    }
}

void Camera::Update() {
    lookAt_ = glm::lookAt(position_, position_ + front_, up_);
}
