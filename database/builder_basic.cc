#include <iostream>

#include <bsoncxx/builder/basic/array.hpp>
#include <bsoncxx/builder/basic/document.hpp>
#include <bsoncxx/builder/basic/kvp.hpp>
#include <bsoncxx/types.hpp>
#include <bsoncxx/json.hpp>

int main() {
    std::cout << "hello builder basic" << std::endl;

    auto doc = bsoncxx::builder::basic::document();
    auto arr = bsoncxx::builder::basic::array();

    using bsoncxx::builder::basic::kvp;

    doc.append(kvp("foo", "bar"));
    doc.append(kvp("baz", bool(false)));
    doc.append(kvp("garply", double(3.14159)));
    doc.append(kvp("a key", "a value"), kvp("another key", "another value"), kvp("moar keys", "moar values"));

    arr.append("hello");
    arr.append(false, true, 1.234);

    using bsoncxx::builder::basic::sub_array;
    using bsoncxx::builder::basic::sub_document;

    doc.append(kvp("subdocument key",
                   [](sub_document subdoc) {
                       subdoc.append(kvp("subdoc key", "subdoc value"), kvp("another subdoc key", 1212));
                   }),
               kvp("subarray key", [](sub_array subarr) {
                   subarr.append(1, false, "hello", 5, [](sub_document subdoc) {
                       subdoc.append(kvp("such", "nesting"), kvp("much", "recurse"));
                   });
               }));

    std::cout << bsoncxx::to_json(doc.view()) << std::endl;

    return 0;
}
