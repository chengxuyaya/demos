#include <iostream>

#include <bsoncxx/builder/basic/array.hpp>
#include <bsoncxx/builder/basic/document.hpp>
#include <bsoncxx/builder/basic/kvp.hpp>
#include <mongocxx/client.hpp>
#include <mongocxx/instance.hpp>

using bsoncxx::builder::basic::kvp;
using bsoncxx::builder::basic::make_array;
using bsoncxx::builder::basic::make_document;

bsoncxx::document::value new_message(int64_t id, int32_t status, std::string msg) {
    return make_document(kvp("uid", id), kvp("status", status), kvp("msg", msg));
}

void insert_test_data(mongocxx::collection &coll) {
    bsoncxx::document::value doc =
        make_document(kvp("messagelist",
            make_array(new_message(413098706, 3, "Lorem ipsum..."),
                new_message(413098707, 2, "Lorem ipsum..."),
                new_message(413098708, 1, "Lorem ipsum..."))));
    coll.insert_one(std::move(doc));
}

void iterate_messagelist(const bsoncxx::document::element &ele) {

}

void print_document(const bsoncxx::document::view &doc) {
    bsoncxx::document::element id_ele = doc["_id"];
}

void iterate_documents(mongocxx::collection &coll) {
    mongocxx::cursor cursor = coll.find({});
    for (const bsoncxx::document::view &doc : cursor) {
        print_document(doc);
    }
}

int main() {
    std::cout << "hello get value from documents" << std::endl;

    auto instance = mongocxx::instance();
    auto client = mongocxx::client(mongocxx::uri());
    mongocxx::collection coll = client["demo"]["events"];

    coll.drop();
    insert_test_data(coll);
    iterate_documents(coll);

    return 0;
}
