#include <iostream>

#include <bsoncxx/builder/stream/array.hpp>
#include <bsoncxx/builder/stream/document.hpp>
#include <bsoncxx/builder/stream/helpers.hpp>
#include <bsoncxx/types.hpp>
#include <bsoncxx/json.hpp>

int main() {
    std::cout << "hello getting values" << std::endl;

    using bsoncxx::builder::stream::open_document;
    using bsoncxx::builder::stream::close_document;
    using bsoncxx::builder::stream::open_array;
    using bsoncxx::builder::stream::close_array;

    auto builder = bsoncxx::builder::stream::document();
    builder << "_id" << 1
        << "name" << open_document
        << "first" << "John"
        << "last" << "Backus"
        << close_document
        << "contribs" << open_array
        << "Fortran" << "ALGOL" <<"Backus-Naur Form" << "FP"
        << close_array
        << "awards" << open_array << open_document
        << "award" << "W.W. McDowell Award"
        << "year" << 1967
        << "by" << "IEEE Computer Society"
        << close_document << open_document
        << "award" << "Draper Prize"
        << "year" << 1993
        << "by" << "National Academy of Engineering"
        << close_document << close_array;

    std::cout << bsoncxx::to_json(builder.view()) << std::endl;;

    auto doc = builder.view();

    bsoncxx::document::element awards = doc["wards"];
    bsoncxx::document::element first_award_year = awards[0]["year"];
    bsoncxx::document::element second_award_year = awards[1]["year"];
    auto last_name = doc["name"]["last"];

    bsoncxx::document::element invalid = doc["name"]["middle"];
    if (invalid) {
        std::cout << "not reached" << std::endl;
    }

    int id = doc["_id"].get_int32().value;
    std::string_view first_name = doc["name"]["first"].get_string().value;

    std::cout << "_id: " << id << " first_name: " << first_name.data() << std::endl;

    return 0;
}
