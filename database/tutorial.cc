#include <iostream>

#include <bsoncxx/builder/basic/document.hpp>
#include <bsoncxx/json.hpp>
#include <mongocxx/client.hpp>
#include <mongocxx/instance.hpp>
#include <mongocxx/uri.hpp>

int main() {
    std::cout << "hello tutorial" << std::endl;

    using bsoncxx::builder::basic::kvp;
    using bsoncxx::builder::basic::make_array;
    using bsoncxx::builder::basic::make_document;

    auto instance = mongocxx::instance();
    auto uri = mongocxx::uri("mongodb://localhost:27017");
    auto client = mongocxx::client(uri);

    mongocxx::database db = client["demo"];
    mongocxx::collection collection = db["test"];

    {
        bsoncxx::document::value doc_value = make_document(
            kvp("name", "MongoDB"),
            kvp("type", "database"),
            kvp("count", 1),
            kvp("versions", make_array("v6.0", "v5.0", "v4.4", "v4.0", "v3.6")),
            kvp("info", make_document(kvp("x", 203), kvp("y", 102)))
        );

        bsoncxx::document::view doc_view = doc_value.view();
        bsoncxx::document::element element = doc_view["name"];
        std::string_view name = element.get_string().value;
        std::cout << "name " << name.data() << std::endl;
    }

    {
        std::optional<mongocxx::result::insert_one> result = collection.insert_one(make_document(kvp("i", 0)));
        if (result) {
            bsoncxx::types::bson_value::view doc_id = result->inserted_id();
            std::cout << "id " << doc_id.get_oid().value.to_string() << std::endl;
        }
    }

    {
        std::vector<bsoncxx::document::value> documents;
        documents.push_back(make_document(kvp("i", 1)));
        documents.push_back(make_document(kvp("i", 2)));

        std::optional<mongocxx::result::insert_many> result = collection.insert_many(documents);
        if (result) {
            bsoncxx::document::element id1 = result->inserted_ids().at(0);
            std::cout << (id1.type() == bsoncxx::type::k_oid) << std::endl;
        }
    }

    return 0;
}
