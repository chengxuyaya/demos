#include <iostream>

#include <bsoncxx/builder/basic/document.hpp>
#include <bsoncxx/json.hpp>
#include <mongocxx/client.hpp>
#include <mongocxx/instance.hpp>
#include <mongocxx/uri.hpp>

mongocxx::instance instance = mongocxx::instance();
mongocxx::client client = mongocxx::client(mongocxx::uri());

int main() {
    using bsoncxx::builder::basic::kvp;
    using bsoncxx::builder::basic::make_array;
    using bsoncxx::builder::basic::make_document;

    std::cout << "hello mongo" << std::endl;
    auto db = client["demo"];
    auto collection = db["user"];

    auto doc = make_document(
        kvp("address", make_document(kvp("street", "yunlonghu"), kvp("zipcode", 110000), kvp("building", "jiuliyihe"),
                                     kvp("coord", make_array(123, 456)))),
        kvp("borough", "Chengxuya"), kvp("cuisine", "China"),
        kvp("grades", make_array(make_document(kvp("date", bsoncxx::types::b_date(std::chrono::milliseconds(123))),
                                               kvp("grade", "A"), kvp("score", 17)))),
        kvp("name", "longhu"), kvp("id", 123456));

    collection.insert_one(doc.view());
    std::cout << bsoncxx::to_json(doc);

    return 0;
}
