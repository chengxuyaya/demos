#include <iostream>
#include <vector>
#include <algorithm>

#include <bsoncxx/array/view.hpp>
#include <bsoncxx/builder/basic/array.hpp>
#include <bsoncxx/builder/basic/kvp.hpp>
#include <bsoncxx/builder/basic/document.hpp>
#include <bsoncxx/json.hpp>
#include <bsoncxx/string/to_string.hpp>

int main() {
    std::cout << "hello view_value" << std::endl;

    using bsoncxx::builder::basic::kvp;
    using bsoncxx::builder::basic::sub_array;

    auto doc = bsoncxx::builder::basic::document();
    doc.append(kvp("team", "platforms"), kvp("id", bsoncxx::types::b_oid()), kvp("members", [](sub_array sa) {
                   sa.append("tyler", "jason", "drew", "sam", "ernie", "john", "mark", "crystal");
               }));

    bsoncxx::document::value value = doc.extract();
    bsoncxx::document::view view = value.view();

    std::cout << bsoncxx::to_json(view) << std::endl;

    for (bsoncxx::document::element ele : view) {
        std::string_view field_key = ele.key();
        std::cout << "Got key: " << field_key << std::endl;

        switch (ele.type()) {
            case bsoncxx::type::k_string:
                std::cout << "stirng" << std::endl;
                break;
            case bsoncxx::type::k_oid:
                std::cout << "object id" << std::endl;
                break;
            case bsoncxx::type::k_array: {
                std::cout << "array" << std::endl;
                bsoncxx::array::view subarr = ele.get_array().value;
                for (bsoncxx::array::element e : subarr) {
                    std::cout << "array element: " << bsoncxx::string::to_string(e.get_string().value) << std::endl;
                }
                break;
            }
            default:
                std::cout << "messed" << std::endl;
        }
    }

    bsoncxx::document::element ele = view["team"];
    if (ele) {
        std::cout << "as expected we have a team" << std::endl;
    }

    size_t num_keys = std::distance(view.begin(), view.end());
    std::cout << "document has " << num_keys << std::endl;

    std::vector<std::string> doc_keys;
    std::transform(view.begin(), view.end(), std::back_inserter(doc_keys),
                   [](bsoncxx::document::element ele) { return bsoncxx::string::to_string(ele.key()); });

    std::cout << "document keys are " << std::endl;
    for (auto key : doc_keys) {
        std::cout << key << " " << std::endl;
    }

    return 0;
}
