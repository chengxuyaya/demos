#include <iostream>

#include <bsoncxx/builder/stream/array.hpp>
#include <bsoncxx/builder/stream/document.hpp>
#include <bsoncxx/builder/stream/helpers.hpp>
#include <bsoncxx/types.hpp>
#include <bsoncxx/json.hpp>

int main() {
    std::cout << "hello builder stream" << std::endl;

    auto doc = bsoncxx::builder::stream::document();
    auto arr = bsoncxx::builder::stream::array();

    doc << "mykey" << "myValue";
    doc << "foo" << false << "baz" << 1234 << "quz" << 1.234;

    arr << 1 << 2 << true;

    using bsoncxx::builder::stream::close_document;
    using bsoncxx::builder::stream::open_document;
    doc << "mySubDoc" << open_document << "subdoc key" << "subdoc value" << close_document;

    using bsoncxx::builder::stream::close_array;
    using bsoncxx::builder::stream::open_array;
    doc << "mySubArr" << open_array << 1 << false << "hello" << close_array;

    using bsoncxx::builder::stream::finalize;
    auto myQuery = bsoncxx::builder::stream::document() << "foo" << "bar" << finalize;

    using bsoncxx::builder::concatenate;
    doc << concatenate(myQuery.view());

    bsoncxx::document::value nestedValue = bsoncxx::builder::stream::document() << "nested" << true << finalize;
    auto topLevelDoc = bsoncxx::builder::stream::document();
    topLevelDoc << "subDoc1" << bsoncxx::types::b_document{nestedValue.view()} << "subDoc2" << open_document
                << concatenate(nestedValue.view()) << close_document << "subDoc3" << nestedValue;

    std::cout << bsoncxx::to_json(doc.view()) << std::endl;
    std::cout << bsoncxx::to_json(topLevelDoc.view()) << std::endl;

    return 0;
}
