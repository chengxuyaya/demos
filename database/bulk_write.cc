#include <iostream>

#include <bsoncxx/builder/basic/document.hpp>
#include <bsoncxx/builder/basic/kvp.hpp>
#include <bsoncxx/json.hpp>
#include <mongocxx/client.hpp>
#include <mongocxx/instance.hpp>
#include <mongocxx/uri.hpp>

int main() {
    std::cout << "hello bulk write" << std::endl;

    auto instance = mongocxx::instance();
    auto client = mongocxx::client(mongocxx::uri());

    auto coll = client["demo"]["coll"];
    coll.drop();

    using bsoncxx::builder::basic::make_document;
    using bsoncxx::builder::basic::kvp;

    auto doc1 = make_document(kvp("a", 1));
    auto doc2 = make_document(kvp("$set", make_document(kvp("a", 2))));

    auto insert_op = mongocxx::model::insert_one(doc1.view());
    auto delete_op = mongocxx::model::delete_one(doc1.view());
    auto upsert_op = mongocxx::model::update_one(doc1.view(), doc2.view());

    upsert_op.upsert(true);

    mongocxx::bulk_write bulk = coll.create_bulk_write();
    bulk.append(insert_op);
    bulk.append(delete_op);
    bulk.append(upsert_op);
    bulk.append(insert_op);
    bulk.append(upsert_op);

    std::optional<mongocxx::result::bulk_write> result = bulk.execute();
    if (!result) {
        return 0;
    }

    std::cout << "upserted ids" << std::endl;
    for (const auto &[index, ele] : result->upserted_ids()) {
        std::cout << "bulk write index: " << index << std::endl
            << (ele.get_oid().value.to_string()) << std::endl;
    }

    mongocxx::cursor cursor = coll.find({});
    for (auto &&doc : cursor) {
    }

    return 0;
}
