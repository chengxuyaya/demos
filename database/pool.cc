#include <iostream>
#include <thread>
#include <vector>
#include <string>
#include <sstream>

#include <bsoncxx/builder/basic/document.hpp>
#include <bsoncxx/builder/basic/kvp.hpp>
#include <bsoncxx/json.hpp>
#include <mongocxx/client.hpp>
#include <mongocxx/instance.hpp>
#include <mongocxx/pool.hpp>
#include <mongocxx/uri.hpp>

int main() {
    std::cout << "hello pool" << std::endl;

    auto instance = mongocxx::instance();
    auto uri = mongocxx::uri("mongodb://localhost:27017/?minPoolSize=3&maxPoolSize=3");
    auto pool = mongocxx::pool(uri);

    std::vector<std::string> collection_names = { "foo", "bar", "baz" };
    std::vector<std::thread> threads;

    using bsoncxx::builder::basic::make_document;
    using bsoncxx::builder::basic::kvp;

    for (int i : { 0, 1, 2 }) {
        auto run = [&](std::int64_t j) {
            mongocxx::pool::entry client = pool.acquire();
            mongocxx::collection coll = (*client)["demo"][collection_names[j]];
            coll.delete_many({});

            auto index = bsoncxx::types::b_int64{j};
            coll.insert_one(make_document(kvp("x", index)));
            if (auto doc = (*client)["demo"][collection_names[j]].find_one({})) {
                std::stringstream ss;
                ss << bsoncxx::to_json(doc->view()) << std::endl;
                std::cout << ss.str();
            }
        };
        std::thread runner(run, i);
        threads.push_back(std::move(runner));
    }

    for (auto &&runner : threads) {
        runner.join();
    }

    return 0;
}
