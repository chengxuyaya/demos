#include <iostream>

#include <bsoncxx/builder/basic/document.hpp>
#include <bsoncxx/builder/basic/kvp.hpp>
#include <mongocxx/instance.hpp>
#include <mongocxx/uri.hpp>
#include <mongocxx/client.hpp>
#include <mongocxx/exception/logic_error.hpp>
#include <mongocxx/exception/error_code.hpp>
#include <mongocxx/exception/operation_exception.hpp>
#include <mongocxx/exception/server_error_code.hpp>
#include <mongocxx/exception/bulk_write_exception.hpp>
#include <bsoncxx/json.hpp>

int main() {
    std::cout << "hello exception" << std::endl;

    auto instance = mongocxx::instance();
    auto client = mongocxx::client(mongocxx::uri());
    auto coll = mongocxx::collection();

    try {
        coll.name();
    } catch (const mongocxx::logic_error &err) {
        std::cout << "using a uninitialized collection throws" << std::endl;
        if (err.code().category() != mongocxx::error_category()) {
        }
        if (err.code() != mongocxx::error_code::k_invalid_collection_object) {
        }
        std::cout << err.what() << std::endl;
    }

    coll = client["demo"]["coll"];
    coll.drop();
    try {
        coll.rename("coll2");
    } catch (const mongocxx::operation_exception &err) {
        std::cout << "renaming a collection that does not exist throws" << std::endl;
        if (err.code().category() != mongocxx::server_error_category()) {}
        std::cout << err.what() << std::endl;
    }

    using bsoncxx::builder::basic::make_document;
    using bsoncxx::builder::basic::kvp;

    bsoncxx::document::value doc = make_document(kvp("_id", 1));
    coll.insert_one(doc.view());
    try {
        coll.insert_one(doc.view());
    } catch (const mongocxx::bulk_write_exception &err) {
        std::cout << "adding a document whose _id is already present throws" << std::endl;
        if (err.code().category() != mongocxx::server_error_category()) {}
        if (err.code().value() != 10000) {
        }
        std::cout << err.what() << std::endl;
        if (err.raw_server_error()) {
            std::cout << "raw server error" << std::endl;
            std::cout << bsoncxx::to_json(*(err.raw_server_error())) << std::endl;
        }
    }

    return 0;
}
