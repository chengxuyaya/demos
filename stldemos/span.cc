/*
 * Copyright (c) 2024 chengxuya.
 * This is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *         http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

#include <array>
#include <iostream>
#include <span>
#include <vector>

struct Bag {
    std::string name;
    int count;
};

void PrintBag(std::span<const Bag> bag) {
    printf("bag address %p\n", bag.data());
    for (const auto &i : bag) {
        std::cout << "name " << i.name << " count " << i.count << std::endl;
        ;
    }
}

int main() {
    std::cout << "hello span" << std::endl;

    Bag a[] = {{"one", 1}, {"two", 2}, {"three", 3}, {"four", 4}, {"five", 5}};

    printf("a address %p\n", a);
    PrintBag(a);
    std::cout << std::endl;

    std::array<Bag, 5> b = {{{"one", 1}, {"two", 2}, {"three", 3}, {"four", 4}, {"five", 5}}};
    printf("b address %p\n", b.data());
    PrintBag(b);
    std::cout << std::endl;

    std::vector<Bag> c = {{"one", 1}, {"two", 2}, {"three", 3}, {"four", 4}, {"five", 5}};
    printf("c address %p\n", c.data());
    PrintBag(c);

    return 0;
}
