/*
 * Copyright (c) 2023 chengxuya.
 * This is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *         http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

#include <atomic>
#include <chrono>
#include <iostream>
#include <thread>
#include <mutex>
#include <condition_variable>

std::mutex gThread;
std::condition_variable gCV;
std::atomic_int gResult = 0;

void WorkerThreadOne() {
    std::this_thread::sleep_for(std::chrono::seconds(1));
    auto id = std::this_thread::get_id();
#ifdef __APPLE__
    printf("one %llu\n", *reinterpret_cast<uint64_t*>(&id));
#else
    printf("one %lu\n", *reinterpret_cast<uint64_t*>(&id));
#endif
    for (int i = 0; i < 10000; ++i) {
        ++gResult;
    }
    if (gResult == 30000) {
        gCV.notify_one();
    }
}

void WorkerThreadTwo() {
    std::this_thread::sleep_for(std::chrono::seconds(1));
    auto id = std::this_thread::get_id();
#ifdef __APPLE__
    printf("one %llu\n", *reinterpret_cast<uint64_t*>(&id));
#else
    printf("one %lu\n", *reinterpret_cast<uint64_t*>(&id));
#endif
    for (int i = 0; i < 10000; ++i) {
        ++gResult;
    }
    if (gResult == 30000) {
        gCV.notify_one();
    }
}

void WorkerThreadThree() {
    std::this_thread::sleep_for(std::chrono::seconds(1));
    auto id = std::this_thread::get_id();
#ifdef __APPLE__
    printf("one %llu\n", *reinterpret_cast<uint64_t*>(&id));
#else
    printf("one %lu\n", *reinterpret_cast<uint64_t*>(&id));
#endif
    for (int i = 0; i < 10000; ++i) {
        ++gResult;
    }
    if (gResult == 30000) {
        gCV.notify_one();
    }
}

int main() {
    std::cout << "start" << std::endl;
    std::thread one(WorkerThreadOne);
    std::thread two(WorkerThreadTwo);
    std::thread three(WorkerThreadThree);
    one.detach();
    two.detach();
    three.detach();

    std::unique_lock<std::mutex> lock(gThread);
    gCV.wait(lock);
    std::cout << gResult << std::endl;
    std::cout << "end" << std::endl;
    return 0;
}
