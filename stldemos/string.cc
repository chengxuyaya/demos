/*
 * Copyright (c) 2024 chengxuya.
 * This is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *         http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

#include <iostream>
#include <string>
#include <fstream>

static constexpr char fileName[] = "out/resource/demos/build_config.json";

void getLine() {
    std::cout << "getline" << std::endl;

    // get from cin
    std::string name;
    std::cout << "what's your name: ";
    std::getline(std::cin, name);
    std::cout << "hello " << name << ", nice to meet you" << std::endl;

    // get from file
    std::ifstream file(fileName);
    std::string line;
    while (std::getline(file, line)) {
        std::cout << line << std::endl;
    }
    std::cout << std::endl;

    // get from file with delimiter
    std::ifstream file2(fileName);
    while (std::getline(file2, line, ':')) {
        std::cout << line << std::endl;
    }
}

int main() {
    std::cout << "hello string" << std::endl;
    getLine();
    return 0;
}
