#include <iostream>
#include <concepts>
#include <string_view>

std::integral auto calcNumber(std::integral auto a, std::integral auto b) {
    return a + b;
}

std::floating_point auto calcNumber(std::floating_point auto a, std::floating_point auto b) {
    return a + b;
}

template <typename T>
requires std::convertible_to<T, std::string_view>
void print(T str) {
    std::cout << "print: " << str << std::endl;
}

template <typename T>
requires std::same_as<T, bool>
void print(T b) {
    std::cout << std::boolalpha << b << std::endl;
}

int main() {
    std::cout << "hello concepts" << std::endl;
    std::cout << "int: " << calcNumber(1, 2) << std::endl;
    std::cout << "float: " << calcNumber(1.1, 2.2) << std::endl;
    std::cout << "long: " << calcNumber(static_cast<long>(1), static_cast<long>(3)) << std::endl;
    std::cout << "double: " << calcNumber(static_cast<double>(3.3), static_cast<double>(9.0)) << std::endl;

    print("hello");
    print(true);

    return 0;
}
