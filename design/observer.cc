/*
 * Copyright (c) 2024 chengxuya.
 * This is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *         http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

#include <algorithm>
#include <chrono>
#include <iostream>
#include <list>
#include <memory>
#include <mutex>
#include <stdx/random.h>
#include <thread>
#include <tuple>

class IObserver {
public:
    virtual void update(std::tuple<float, float, float> measurement) = 0;
};

class ISubject {
public:
    virtual void registerObserver(std::shared_ptr<IObserver> observer) = 0;
    virtual void removeObserver(std::shared_ptr<IObserver> observer) = 0;
    virtual void notifyObservers() = 0;
};

class IDisplayElement {
public:
    virtual void display() = 0;
};

class WeatherData : public ISubject {
public:
    void registerObserver(std::shared_ptr<IObserver> observer) override {
        if (observer != nullptr) {
            std::lock_guard guard(observerMutex_);
            observers_.push_back(observer);
        }
    }

    void removeObserver(std::shared_ptr<IObserver> observer) override {
        std::lock_guard guard(observerMutex_);
        observers_.remove_if([](auto obs) { return obs != nullptr; });
    }

    void notifyObservers() override {
        std::lock_guard guard(observerMutex_);
        std::for_each(observers_.cbegin(), observers_.cend(), [this](auto obs) { obs->update(measurement_); });
    }

public:
    void setMeasurement(std::tuple<float, float, float> measurement) {
        measurement_ = measurement;
    }

private:
    std::tuple<float, float, float> measurement_;
    std::list<std::shared_ptr<IObserver>> observers_;
    std::mutex observerMutex_;
};

void weatherStationThread(std::shared_ptr<WeatherData> data) {
    while (true) {
        data->setMeasurement(
            std::make_tuple(stdx::get_random(-40, 40), stdx::get_random(10, 100), stdx::get_random(0, 50)));
        data->notifyObservers();
        std::this_thread::sleep_for(std::chrono::seconds(1));
    }
}

class CurrentConditionDisplay : public IObserver, public IDisplayElement {
public:
    void update(std::tuple<float, float, float> measurement) override {
        std::tie(temperature_, humidity_, pressure_) = measurement;
        display();
    }

    void display() override {
        std::cout << "currentCondition temperature: " << temperature_ << " humidity: " << humidity_
                  << " pressure: " << pressure_ << std::endl;
        ;
    }

private:
    float temperature_ = 0;
    float humidity_ = 0;
    float pressure_ = 0;
};

class StatisticsDisplay : public IObserver, public IDisplayElement {
public:
    void update(std::tuple<float, float, float> measurement) override {
        std::tie(temperature_, humidity_, pressure_) = measurement;
        display();
    }

    void display() override {
        std::cout << "statistics temperature: " << temperature_ << " humidity: " << humidity_
                  << " pressure: " << pressure_ << std::endl;
        ;
    }

private:
    float temperature_ = 0;
    float humidity_ = 0;
    float pressure_ = 0;
};

int main() {
    auto currentCondition = std::make_shared<CurrentConditionDisplay>();
    auto statistics = std::make_shared<StatisticsDisplay>();

    auto weatherData = std::make_shared<WeatherData>();

    weatherData->registerObserver(currentCondition);
    weatherData->registerObserver(statistics);

    std::thread thread(weatherStationThread, weatherData);
    thread.join();

    return 0;
}
