/*
 * Copyright (c) 2024 chengxuya.
 * This is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *         http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

#include <iostream>
#include <memory>
#include <string>
#include <string_view>

class IFlyBehavior {
public:
    virtual void Fly() = 0;
};

class IQuackBehavior {
public:
    virtual void Quack() = 0;
};

class Duck {
public:
    Duck(std::string_view name, std::shared_ptr<IFlyBehavior> flyBehavior,
         std::shared_ptr<IQuackBehavior> quackBehavior)
        : name_(name), flyBehavior_(flyBehavior), quackBehavior_(quackBehavior) {
    }
    virtual ~Duck() = default;

    virtual void PerformFly() final {
        if (flyBehavior_ != nullptr) {
            flyBehavior_->Fly();
        }
    }
    virtual void PerformQuack() final {
        if (quackBehavior_ != nullptr) {
            quackBehavior_->Quack();
        }
    }

    virtual void Swim() = 0;
    virtual void Display() = 0;

protected:
    std::string name_;

private:
    std::shared_ptr<IFlyBehavior> flyBehavior_ = nullptr;
    std::shared_ptr<IQuackBehavior> quackBehavior_ = nullptr;
};

class FlyWithWings : public IFlyBehavior {
public:
    void Fly() override {
        std::cout << "fly with wings" << std::endl;
    }
};

class FlyNoWay : public IFlyBehavior {
public:
    void Fly() override {
        std::cout << "fly no way" << std::endl;
    }
};

class BigQuack : public IQuackBehavior {
public:
    void Quack() override {
        std::cout << "big quack" << std::endl;
    }
};

class MuteQuack : public IQuackBehavior {
public:
    void Quack() override {
        std::cout << "mute quack" << std::endl;
    }
};

class WookDuck : public Duck {
public:
    WookDuck(std::string_view name) : Duck(name, std::make_shared<FlyNoWay>(), std::make_shared<MuteQuack>()) {
    }

    void Swim() override {
        std::cout << name_ << " swim" << std::endl;
    }
    void Display() override {
        std::cout << name_ << " display" << std::endl;
    }
};

class Duckyou : public Duck {
public:
    Duckyou(std::string_view name) : Duck(name, std::make_shared<FlyWithWings>(), std::make_shared<BigQuack>()) {
    }

    void Swim() override {
        std::cout << name_ << " swim" << std::endl;
    }
    void Display() override {
        std::cout << name_ << " display" << std::endl;
    }
};

int main() {
    std::cout << "hello" << std::endl;
    auto wookDuck = std::make_unique<WookDuck>("wookduck");
    auto duckyou = std::make_unique<Duckyou>("duckyou");
    wookDuck->Swim();
    wookDuck->Display();
    duckyou->Swim();
    duckyou->Display();

    return 0;
}
