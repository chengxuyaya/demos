/*
 * Copyright (c) 2023 chengxuya.
 * This is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *         http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

#include <array>
#include <cstdint>

#include <node/node_api.h>

#include "base_context.h"
#include "configuration.h"
#include "set_iface_config.h"

namespace {

void DeclareEthernetInterface(napi_env env, napi_value exports) {
    napi_property_descriptor setIfaceConfig = {
        .utf8name = "setIfaceConfig",
        .name = nullptr,
        .method = SetIfaceConfig::Do,
        .getter = nullptr,
        .setter = nullptr,
        .value = nullptr,
        .attributes = napi_writable,
        .data = nullptr,
    };
    std::array<napi_property_descriptor, 1> desc = {
        setIfaceConfig,
    };
    napi_define_properties(env, exports, desc.size(), desc.data());
}

void DeclareEthernetData(napi_env env, napi_value exports) {
    napi_value staticMode = nullptr;
    napi_value dhcpMode = nullptr;
    napi_create_int32(env, static_cast<int32_t>(IPSetMode::STATIC), &staticMode);
    napi_create_int32(env, static_cast<int32_t>(IPSetMode::DHCP), &dhcpMode);

    napi_property_descriptor staticModeDesc = {
        .utf8name = "STATIC",
        .name = nullptr,
        .method = nullptr,
        .getter = nullptr,
        .setter = nullptr,
        .value = staticMode,
        .attributes = napi_default,
        .data = nullptr,
    };
    napi_property_descriptor dhcpModeDesc = {
        .utf8name = "DHCP",
        .name = nullptr,
        .method = nullptr,
        .getter = nullptr,
        .setter = nullptr,
        .value = dhcpMode,
        .attributes = napi_default,
        .data = nullptr,
    };
    std::array<napi_property_descriptor, 2> desc = {staticModeDesc, dhcpModeDesc};
    napi_define_properties(env, exports, desc.size(), desc.data());
}

napi_value RegisterEthernetInterface(napi_env env, napi_value exports) {
    DeclareEthernetInterface(env, exports);
    DeclareEthernetData(env, exports);
    return exports;
}

napi_module ethernetModule = {
    .nm_version = 1,
    .nm_flags = 0,
    .nm_filename = nullptr,
    .nm_register_func = RegisterEthernetInterface,
    .nm_modname = "net.ethernet",
    .nm_priv = ((void*)0),
    .reserved = {0},
};

}  // namespace

extern "C" __attribute__((constructor)) void RegisterEthernetModule() {
    napi_module_register(&ethernetModule);
}
