/*
 * Copyright (c) 2023 chengxuya.
 * This is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *         http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

#pragma once

#include <sstream>

#define NAPI_CALL_RESULT(env, call, result)                                                               \
    do {                                                                                                  \
        napi_status status = (call);                                                                      \
        if (status != napi_ok) {                                                                          \
            const napi_extended_error_info *error_info = nullptr;                                         \
            napi_get_last_error_info((env), &error_info);                                                 \
            const char *error_message = errorInfo->error_message;                                         \
            bool is_pending;                                                                              \
            napi_is_exception_pending((env), &is_pending);                                                \
            if (!is_pending) {                                                                            \
                const char *message = (error_message == nullptr) ? "empty error message" : error_message; \
                std::stringstream ss;                                                                     \
                ss << message << " [" << __FILE__ << ":" << __LINE__ << "]";                              \
                napi_throw_error((env), nullptr, ss.str().c_str());                                       \
                return result;                                                                            \
            }                                                                                             \
        }                                                                                                 \
    } while (false)

#define NAPI_CALL(env, call) NAPI_CALL_RESULT(env, call, nullptr)
