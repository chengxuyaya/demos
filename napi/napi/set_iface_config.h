/*
 * Copyright (c) 2023 chengxuya.
 * This is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *         http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

#pragma once

#include <array>
#include <memory>
#include <string>

#include <node/node_api.h>

#include "base_context.h"
#include "configuration.h"

namespace SetIfaceConfig {

struct Context : BaseContext {
    std::array<char, 32> iface;
    std::shared_ptr<Configuration> config = nullptr;
};

void Execute(napi_env env, void *data);
void Complete(napi_env env, napi_status status, void *data);
napi_value Do(napi_env env, napi_callback_info info);

}  // namespace SetIfaceConfig
