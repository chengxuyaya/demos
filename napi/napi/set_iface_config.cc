/*
 * Copyright (c) 2023 chengxuya.
 * This is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *         http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

#include "set_iface_config.h"

#include "ethernet.h"

namespace SetIfaceConfig {

Context *ParseParams(napi_env env, napi_callback_info info) {
    Context *context = new Context;
    size_t argc = 3;
    napi_value argv[] = {nullptr, nullptr, nullptr};
    napi_get_cb_info(env, info, &argc, argv, nullptr, nullptr);
    napi_get_value_string_utf8(env, argv[0], context->iface.data(), context->iface.size(), 0);

    napi_value mode = nullptr;
    napi_value ipAddr = nullptr;
    napi_value route = nullptr;
    napi_value gateway = nullptr;
    napi_value netMask = nullptr;
    napi_value dnsServer = nullptr;
    napi_value domain = nullptr;

    std::shared_ptr<Configuration> config = std::make_shared<Configuration>();
    napi_get_named_property(env, argv[1], "mode", &mode);
    napi_get_value_int32(env, mode, reinterpret_cast<int32_t *>(&(config->mode)));
    if (config->mode == IPSetMode::STATIC) {
        std::array<char, 64> buf;

        napi_get_named_property(env, argv[1], "ipAddr", &ipAddr);
        napi_get_named_property(env, argv[1], "route", &route);
        napi_get_named_property(env, argv[1], "gateway", &gateway);
        napi_get_named_property(env, argv[1], "netMask", &netMask);
        napi_get_named_property(env, argv[1], "dnsServer", &dnsServer);
        napi_get_named_property(env, argv[1], "domain", &domain);

        napi_get_value_string_utf8(env, ipAddr, buf.data(), buf.size(), 0);
        config->ipStatic.ipAddr = buf.data();
        napi_get_value_string_utf8(env, route, buf.data(), buf.size(), 0);
        config->ipStatic.route = buf.data();
        napi_get_value_string_utf8(env, gateway, buf.data(), buf.size(), 0);
        config->ipStatic.gateway = buf.data();
        napi_get_value_string_utf8(env, netMask, buf.data(), buf.size(), 0);
        config->ipStatic.netMask = buf.data();
        napi_get_value_string_utf8(env, dnsServer, buf.data(), buf.size(), 0);
        config->ipStatic.dnsServer = buf.data();
        napi_get_value_string_utf8(env, domain, buf.data(), buf.size(), 0);
        config->ipStatic.domain = buf.data();
    }
    context->config = config;

    if (argc == 2) {
        context->isPromise = true;
        return context;
    }
    if (argc == 3) {
        context->isPromise = false;
        context->callback = argv[2];
    }
    return context;
}

void Execute(napi_env env, void *data) {
    Context *context = reinterpret_cast<Context *>(data);
    std::unique_ptr<Ethernet> ethernet = std::make_unique<Ethernet>();
    if (ethernet->SetIfaceConfig(context->iface.data(), context->config) != 0) {
        context->errorCode = 1;
    }
}

void Complete(napi_env env, napi_status status, void *data) {
    Context *context = reinterpret_cast<Context *>(data);
    napi_value errorCode = nullptr;
    napi_create_int32(env, context->errorCode, &errorCode);
    if (context->isPromise) {
        if (context->errorCode == 0) {
            napi_resolve_deferred(env, context->deferred, errorCode);
        } else {
            napi_reject_deferred(env, context->deferred, errorCode);
        }
    } else {
        napi_value callback = nullptr;
        napi_get_reference_value(env, context->callbackRef, &callback);
        napi_value argv[] = {errorCode};
        napi_value undefined = nullptr;
        napi_get_undefined(env, &undefined);
        napi_call_function(env, undefined, callback, 1, argv, nullptr);
        napi_delete_reference(env, context->callbackRef);
    }
    napi_delete_async_work(env, context->asyncWork);
    delete context;
}

napi_value Do(napi_env env, napi_callback_info info) {
    Context *context = ParseParams(env, info);
    napi_value promise = nullptr;

    if (context->isPromise) {
        napi_create_promise(env, &context->deferred, &promise);
    } else {
        napi_get_undefined(env, &promise);
        napi_create_reference(env, context->callback, 1, &context->callbackRef);
    }

    napi_value asyncName = nullptr;
    napi_create_string_utf8(env, "setIfaceConfig", NAPI_AUTO_LENGTH, &asyncName);
    napi_create_async_work(env, nullptr, asyncName, Execute, Complete, reinterpret_cast<void *>(context),
                           &context->asyncWork);
    napi_queue_async_work(env, context->asyncWork);

    return promise;
}

}  // namespace SetIfaceConfig
