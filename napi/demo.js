/*
 * Copyright (c) 2023 chengxuya.
 * This is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *         http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

const { setIfaceConfig, STATIC, DHCP } = require("../../out/lib/libethernet.node");

const config = {
  ipAddr: "ipaddr",
  route: "route",
  gateway: "gateway",
  netMask: "netMask",
  dnsServer: "dnsServer",
  domain: "domain",
};

setIfaceConfig("iface", config, (result) => {
  console.log(result);
});
