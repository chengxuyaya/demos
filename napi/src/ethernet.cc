/*
 * Copyright (c) 2023 chengxuya.
 * This is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *         http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

#include "ethernet.h"

#include <fstream>
#include <sstream>
#include <thread>

namespace {
constexpr char FILE_NAME[] = "iface_config.txt";
}  // namespace

int32_t Ethernet::SetIfaceConfig(const std::string &iface, std::shared_ptr<Configuration> config) {
    std::this_thread::sleep_for(std::chrono::seconds(3));
    std::vector<std::string> infos(7);
    infos.clear();
    if (config->mode == IPSetMode::STATIC) {
        infos.push_back(iface);
        infos.push_back("static");
        infos.push_back(config->ipStatic.ipAddr);
        infos.push_back(config->ipStatic.route);
        infos.push_back(config->ipStatic.gateway);
        infos.push_back(config->ipStatic.netMask);
        infos.push_back(config->ipStatic.dnsServer);
        infos.push_back(config->ipStatic.domain);
    } else {
        infos.push_back("dhcp");
    }

    try {
        std::ofstream file;
        file.open(FILE_NAME);
        for (const auto &info : infos) {
            file << info << std::endl;
        }
        file.close();
    } catch (std::ofstream::failure &error) {
        return 1;
    }

    return 0;
}

std::shared_ptr<Configuration> Ethernet::GetIfaceConfig(const std::string &iface) {
    std::string infos;
    try {
        std::ifstream file;
        file.open(FILE_NAME);
        std::stringstream ss;
        ss << file.rdbuf();
        infos = ss.str();
        file.close();
    } catch (std::ifstream::failure &error) {
    }

    // std::vector<std::string> result = utility::Split(infos, "\n");
    std::vector<std::string> result;
    std::shared_ptr<Configuration> config = std::make_shared<Configuration>();
    if (result.size() == 8 && result.at(1) == "static") {
        config->mode = IPSetMode::STATIC;
        StaticConfiguration ipStatic = {
            .ipAddr = result.at(2),
            .route = result.at(3),
            .gateway = result.at(4),
            .netMask = result.at(5),
            .dnsServer = result.at(6),
            .domain = result.at(7),
        };
    } else {
        config->mode = IPSetMode::DHCP;
    }
    return config;
}

int32_t Ethernet::IsIfaceActive(const std::string &iface) {
    return 3;
}

std::vector<std::string> Ethernet::GetAllActiveIfaces() {
    return {"one", "two", "three"};
}
