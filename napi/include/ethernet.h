/*
 * Copyright (c) 2023 chengxuya.
 * This is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *         http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

#pragma once

#include <cstdint>
#include <memory>
#include <string>
#include <vector>

#include "configuration.h"

class Ethernet {
public:
    Ethernet() = default;
    ~Ethernet() = default;

    int32_t SetIfaceConfig(const std::string &iface, std::shared_ptr<Configuration> config);
    std::shared_ptr<Configuration> GetIfaceConfig(const std::string &iface);

    int32_t IsIfaceActive(const std::string &iface);
    std::vector<std::string> GetAllActiveIfaces();
};
