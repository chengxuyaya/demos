/*
 * Copyright (c) 2023 chengxuya.
 * This is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *         http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

#include <iostream>

#ifdef _WIN32
#include <windows.h>
#else
#include <dlfcn.h>
#endif

int main() {
#ifdef _WIN32
    HINSTANCE handle = LoadLibrary("out/lib/libdlopenlib.dll");
    if (handle == nullptr) {
        return 1;
    }

    std::cout << "open dll successful" << std::endl;
    FARPROC func = GetProcAddress(handle, "world");
    if (func != nullptr) {
        func();
    }
    FreeLibrary(handle);
#else
    void *handle = dlopen("out/lib/libdlopenlib.so", RTLD_LAZY);
    if (handle == nullptr) {
        std::cout << dlerror() << std::endl;
        return 1;
    }

    std::cout << "open lib successful" << std::endl;
    auto fun = reinterpret_cast<void (*)()>(dlsym(handle, "world"));
    fun();

    dlclose(handle);
    std::cout << "close lib successful" << std::endl;
#endif
    return 0;
}
