/*
 * Copyright (c) 2023 chengxuya.
 * This is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *         http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

#include <iostream>

enum class Symbol : int {
    TRIANGLE = 0x1,
    SQUARE = 0x2,
    CIRCLE = 0x3,
};

Symbol operator&(Symbol a, Symbol b) {
    return static_cast<Symbol>(static_cast<int>(a) & static_cast<int>(b));
}

Symbol operator|(Symbol a, Symbol b) {
    return static_cast<Symbol>(static_cast<int>(a) | static_cast<int>(b));
}

void Print(Symbol symbol) {
    std::cout << "====begin====" << std::endl;

    if ((symbol & Symbol::TRIANGLE) == Symbol::TRIANGLE) {
        std::cout << "hello triangle" << std::endl;
    }

    if ((symbol & Symbol::SQUARE) == Symbol::SQUARE) {
        std::cout << "hello square" << std::endl;
    }

    if ((symbol & Symbol::CIRCLE) == Symbol::CIRCLE) {
        std::cout << "hello circle" << std::endl;
    }

    std::cout << "====end====" << std::endl << std::endl;
    ;
}

int main() {
    Symbol triangle = Symbol::TRIANGLE;
    Symbol square = Symbol::SQUARE;
    Symbol circle = Symbol::CIRCLE;
    Symbol triangle_square = Symbol::TRIANGLE | Symbol::SQUARE;
    Symbol triangle_circle = Symbol::TRIANGLE | Symbol::CIRCLE;
    Symbol square_circle = Symbol::SQUARE | Symbol::CIRCLE;
    Symbol triangle_square_circle = Symbol::TRIANGLE | Symbol::SQUARE | Symbol::CIRCLE;

    Print(triangle);
    Print(square);
    Print(circle);
    Print(triangle_square);
    Print(triangle_circle);
    Print(square_circle);
    Print(triangle_square_circle);

    return 0;
}
