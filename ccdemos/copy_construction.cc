/*
 * Copyright (c) 2023 chengxuya.
 * This is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *         http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

#include <cstddef>
#include <cstring>
#include <iostream>
#include <memory>
#include <string>

class Key {
public:
    Key();
    explicit Key(const std::string &key);
    explicit Key(const char *key);
    Key(const Key &key);
    Key(Key &&key);
    ~Key();

    Key &operator=(Key &&key);
    Key &operator=(const Key &key);

public:
    const char *Data() const;
    size_t Length() const;

private:
    std::unique_ptr<char[]> mData = nullptr;
    size_t mLength = 0;
};

Key::Key() : mData(std::make_unique<char[]>(0)) {
    std::cout << "default constructor" << std::endl;
}

Key::Key(const std::string &key) : mData(std::make_unique<char[]>(key.size())), mLength(key.size()) {
    std::cout << "std::string constructor" << std::endl;
    strcpy(mData.get(), key.c_str());
}

Key::Key(const char *key) : mData(std::make_unique<char[]>(strlen(key))), mLength(strlen(key)) {
    std::cout << "const char* constructor" << std::endl;
    strcpy(mData.get(), key);
}

Key::Key(const Key &key) {
    std::cout << "left value Key constructor" << std::endl;
    if (this != &key) {
        *this = key;
    }
}

Key::Key(Key &&key) {
    std::cout << "right value Key constructor" << std::endl;
    *this = key;
}

Key::~Key() {
    std::cout << "destructor" << std::endl;
    memset(mData.get(), 0, mLength);
}

Key &Key::operator=(const Key &key) {
    std::cout << "left value key =" << std::endl;
    if (this != &key) {
        mData = std::make_unique<char[]>(key.Length());
        mLength = key.Length();
        strcpy(mData.get(), key.Data());
    }
    return *this;
}

Key &Key::operator=(Key &&key) {
    std::cout << "right value key =" << std::endl;
    mData = std::make_unique<char[]>(key.Length());
    mLength = key.Length();
    strcpy(mData.get(), key.Data());
    return *this;
}

const char *Key::Data() const {
    return mData.get();
}

size_t Key::Length() const {
    return mLength;
}

int main() {
    std::string chaos = "afewiytnxchvoayweirhapsodifhoaiwghthoihasdpfja";
    const char *hello = "hello world";

    {
        // default constructor.
        Key key;
    }
    {
        // constructor.
        Key key(chaos);
    }
    { Key key(hello); }
    {
        // copy constructor.
        Key key;
        Key key1 = key;
    }
    {
        // ==
        Key key;
        Key key1;
        key1 = key;
    }

    return 0;
}
